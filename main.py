import os
from dotenv import load_dotenv
import requests
from datetime import datetime, date
import re
from ast import literal_eval
import pandas as pd
from tqdm import tqdm
import time

load_dotenv()

RASA_X_USER = os.environ.get("RASA_X_USER", "me")
RASA_X_PASSWORD = os.environ.get("RASA_X_PASSWORD")
RASA_X_BASE_URL = os.environ.get("RASA_X_BASE_URL")
RASA_X_AUTH_URL = RASA_X_BASE_URL + "/api/auth"
RASA_X_LIST_CONV_URL = RASA_X_BASE_URL + "/api/conversations"
RASA_X_TRACKER_URL = RASA_X_BASE_URL + "/api/conversations/{}"
START_DATE = os.environ.get("START_DATE")
END_DATE = os.environ.get("END_DATE")


def get_timestamp(timestamp: int, format: str):
    """

    :param timestamp:
    :param format: %Y-%m-%d %H:%M:%S
    :return:
    """
    readable_timestamp = datetime.fromtimestamp(timestamp).strftime(format)
    return readable_timestamp


def login_rasa_x(username, password):
    data = {"username": username, "password": password}
    resp = requests.post(RASA_X_AUTH_URL, json=data)
    if resp.status_code == 200:
        return resp.json()["access_token"]
    raise Exception("Wrong username or password")


def get_list_conversation_id(start_st, end_st, token):
    # psid is page-scope id.
    # salebot use psid as conversation_id for rasa
    headers = {"Authorization": "Bearer {}".format(token)}
    params = {"start": start_st, "until": end_st}
    resp = requests.get(RASA_X_LIST_CONV_URL, headers=headers, params=params)

    if resp.status_code == 200:
        data = resp.json()
        return [conv["sender_id"] for conv in data]
    raise Exception("Failed to get list converstation")


def get_tracker(conversation_id, token):
    headers = {"Authorization": "Bearer {}".format(token)}
    url = RASA_X_TRACKER_URL.format(conversation_id)
    try:
        resp = requests.get(url, headers=headers)
        if resp.status_code == 200:
            data = resp.json()
            return data
    except:
        print("Failed to get tracker of converstation {}".format(conversation_id))
        return ""

    # raise Exception(
    #     "Failed to get tracker of converstation {}".format(conversation_id)
    # )


def get_range_time_of_yesterday(today):
    start_dt = datetime(today.year, today.month, today.day, 0, 0, 0)
    end_dt = datetime(today.year, today.month, today.day, 23, 59, 59)
    return int(start_dt.timestamp()), int(end_dt.timestamp())


def parse_time(text):
    match = re.search(r"\d{4}-\d{2}-\d{2}", text)
    date_dt = datetime.strptime(match.group(), "%Y-%m-%d")
    return date_dt


def process_raw_rasa_chatlog(data: list, date_list=None, year_list=None):
    fmt = "%Y-%m-%d %H:%M:%S"

    if date_list is None:
        date_list = []
    if year_list is None:
        year_list = []

    df_data = {
        "message_id": [],
        "sender_id": [],
        "sender": [],
        "user_message": [],
        "bot_message": [],
        "utter_name": [],
        "intent": [],
        "entities": [],
        "created_time": [],
        "attachments": [],
    }

    for item in tqdm(data):
        if item == "":
            continue
        if item["events"] is not None and str(item["events"]) != "nan":
            events = item["events"]
            if not isinstance(item["events"], list):
                events = literal_eval(item["events"])
        else:
            continue
        sender_id = item["sender_id"]
        user_bot_events = [x for x in events if x["event"] == "user" or x["event"] == "bot"]

        for event_index, event in enumerate(user_bot_events):
            timestamp = get_timestamp(int(event["timestamp"]), fmt)
            timestamp_year = get_timestamp(int(event["timestamp"]), "%Y")
            timestamp_date = get_timestamp(int(event["timestamp"]), "%Y-%m-%d")
            message_id = ""
            user_intent = ""
            # if timestamp_date in date_list or timestamp_year in year_list:
            if date_list[0] <= datetime.strptime(timestamp_date, "%Y-%m-%d") < date_list[1]:
                entity_list = ""
                if "metadata" in event:
                    if "utter_name" in event["metadata"]:
                        df_data["utter_name"].append(event["metadata"]["utter_name"])
                    else:
                        df_data["utter_name"].append("")
                else:
                    df_data["utter_name"].append("")

                if "parse_data" in event:
                    if "entities" in event["parse_data"]:
                        entities = event["parse_data"]["entities"]
                        if entities:
                            for item in entities:
                                if "value" in item:
                                    if item["value"] is not None:
                                        entity_list += str(item["value"]) + ","
                    if "intent" in event["parse_data"]:
                        if "name" in event["parse_data"]["intent"]:
                            user_intent = event['parse_data']['intent']['name']

                if "message_id" in event:
                    message_id = event["message_id"]

                message = event["text"]
                attachments = ""
                if message is None:
                    message = ""

                if "scontent" in message:
                    messsage_list = message.split("\n")
                    text_message = ""
                    for item in messsage_list:
                        if "scontent" in item:
                            attachments += item + ", "
                        else:
                            text_message += item + " "
                    message = text_message

                df_data["entities"].append(entity_list)
                df_data["sender"].append(event["event"])
                df_data["intent"].append(user_intent)
                df_data["message_id"].append(message_id)
                df_data["sender_id"].append(sender_id)
                df_data["created_time"].append(timestamp)
                df_data["attachments"].append(attachments)

                event_owner = event["event"]
                if event_owner == "user":
                    df_data["user_message"].append(message)
                    df_data["bot_message"].append("")
                else:
                    df_data["user_message"].append("")
                    df_data["bot_message"].append(message)

    rasa_chatlog_df = pd.DataFrame.from_dict(df_data)
    rasa_chatlog_df.to_csv("data_oct.csv", index=False)
    return rasa_chatlog_df


def main():
    start_time = time.time()
    token = login_rasa_x(RASA_X_USER, RASA_X_PASSWORD)
    start_st = int(datetime.strptime(START_DATE, "%Y-%m-%d").timestamp())
    end_st = int(datetime.strptime(END_DATE, "%Y-%m-%d").timestamp())
    conv_ids = get_list_conversation_id(start_st, end_st, token)

    if conv_ids:
        data = [get_tracker(conv_id, token) for conv_id in conv_ids]
        process_raw_rasa_chatlog(data, [datetime.strptime(START_DATE, "%Y-%m-%d"), datetime.strptime(END_DATE, "%Y-%m-%d")], ["2020"])

    print("Run time: " + str(time.time() - start_time))


main()
